<?php

$this->title = 'Создание записи';
?>

<div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
