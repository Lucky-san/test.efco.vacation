(function() {
  const $calendar = document.getElementById('js-fullcalendar');

  if (!$calendar) return;

  const userId = $calendar.dataset.userId;
  const userName = $calendar.dataset.userName;
  const isQuest = +userId === 0 || userName === '';
  let calendar = null;

  const onAddEvent = function(event) {
    const create = confirm('Создать событие?');

    if (create) {
      const onAddEventSuccess = function(json) {
        calendar.addEvent({
          id: json['id'],
          uid: userId,
          title: userName,
          start: event.start,
          end: event.end,
          allDay: event.allDay,
        });
      };

      const start = +event.start/1e3;
      const end = +event.end/1e3;
      fetch(`/index.php?r=vacation/add-event&start=${start}&end=${end}`)
        .then(response => response.json())
        .then(onAddEventSuccess)
        .catch(error => console.error(error));
    }
    calendar.unselect();
  };

  const onUpdateEvent = function(info) {
    const event = info.event;
    const id = event.id;
    const start = +event.start/1e3;
    const end = +event.end/1e3;

    fetch(`/index.php?r=vacation/update-event&id=${id}&start=${start}&end=${end}`)
      .then(response => response.json())
      .then(json => console.log(json))
      .catch(error => console.error(error));
  };

  calendar = new FullCalendar.Calendar($calendar, {
    plugins: [ 'interaction', 'dayGrid', 'list' ],
    header: {
      left: 'prev, next',
      center: 'title',
      right: 'today, listMonth, dayGridMonth',
    },
    fixedWeekCount: false,
    locale: 'ru',
    editable: true, // for interaction
    navLinks: false, // can click day/week names to navigate views
    eventLimit: true, // allow "more" link when too many events
    selectable: isQuest ? false : true, // for add event
    select: onAddEvent,
    events: {
      url: '/index.php?r=vacation/get-events',
    },
    loading: function(isLoading) {
      isLoading
        ? $calendar.classList.add('is-loading')
        : $calendar.classList.remove('is-loading');
    },
    eventResize: onUpdateEvent,
    eventDrop: onUpdateEvent,
  });

  calendar.render();
})();