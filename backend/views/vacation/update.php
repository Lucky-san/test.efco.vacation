<?php

$this->title = 'Редактирование записи';
?>

<div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
