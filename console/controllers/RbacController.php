<?php
namespace console\controllers;

use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
  public function actionInit()
  {
    $authManager = Yii::$app->authManager;
    $authManager->removeAll();

    $canApprove = $authManager->createPermission('canApprove');
    $authManager->add($canApprove);

    $admin = $authManager->createRole('admin');
    $authManager->add($admin);
    $authManager->addChild($admin, $canApprove);

    $authManager->assign($admin, 1);
  }
}