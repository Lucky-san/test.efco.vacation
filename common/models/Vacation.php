<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class Vacation extends ActiveRecord
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const APPROVE_INACTIVE = 0;
    const APPROVE_ACTIVE = 1;

    public $start_date;
    public $end_date;
    public $username;

    public static function tableName()
    {
        return '{{%vacation}}';
    }

    public function rules()
    {
        return [
            [['start_date', 'end_date', 'username'], 'trim'],
            [['start_date', 'end_date'], Yii::$app->requestedAction->id === 'create' ? 'required' : 'safe'],
            [['start_date', 'end_date'], 'date', 'format' => 'php:Y-m-d'],
            [['start', 'end'], 'safe'],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE]],
            ['approve', 'in', 'range' => [self::APPROVE_ACTIVE, self::APPROVE_INACTIVE]],
            [['username'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'start_date' => 'Начало отпуска',
            'end_date' => 'Конец отпуска',
            'status' => 'Статус видимости',
            'approve' => 'Статус согласования',
            'username' => 'Имя',
        ];
    }

    public function publish()
    {
        $this->status = self::STATUS_ACTIVE;
        $this->save();
    }

    public function unpublish()
    {
        $this->status = self::STATUS_INACTIVE;
        $this->save();
    }

    public function approve()
    {
        $this->approve = self::APPROVE_ACTIVE;
        $this->save();
    }

    public function unapprove()
    {
        $this->approve = self::APPROVE_INACTIVE;
        $this->save();
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function checkActionAccess()
    {
        if (Yii::$app->user->can('admin')) return true;
        if ($this->user->id === Yii::$app->user->id && $this->approve !== 1) return true;
        return false;
    }

    public static function getEvents()
    {
        return self::find()
            ->alias('v')
            ->joinWith('user u')
            ->where(['v.status' => self::STATUS_ACTIVE])
            ->asArray()
            ->all();
    }
}
