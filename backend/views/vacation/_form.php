<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use kartik\date\DatePicker;
?>

<?php $form = ActiveForm::begin() ?>
    <div class="row">
        <div class="col-md-6">
            <?=DatePicker::widget([
                'model' => $model,
                'attribute' => 'start_date',
                'attribute2' => 'end_date',
                'separator' => '<i class="fa fa-arrows-h" aria-hidden="true"></i>',
                'options' => [
                    'placeholder' => 'Начальная дата',
                    'autocomplete' => 'off',
                    'value' => date('Y-m-d', $model->start),
                ],
                'options2' => [
                    'placeholder' => 'Конечная дата',
                    'autocomplete' => 'off',
                    'value' => date('Y-m-d', $model->end),
                ],
                'type' => DatePicker::TYPE_RANGE,
                'form' => $form,
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd',
                    'autoclose' => true,
                ]
            ]); ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

<?php $form = ActiveForm::end() ?>
