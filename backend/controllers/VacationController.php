<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use common\models\Vacation;
use common\models\VacationSearch;

class VacationController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [],
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new VacationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new Vacation();

        if ($model->load(Yii::$app->request->post())) {
            $model->user_id = Yii::$app->user->id;
            $model->start = Yii::$app->formatter->asTimestamp($model->start_date);
            $model->end = Yii::$app->formatter->asTimestamp($model->end_date);
            $model->status = 1;
            $model->approve = 0;
            $model->save();

            return $this->redirect(['index']);
        }

        $model->start = time();
        $model->end = $model->start;

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->start = Yii::$app->formatter->asTimestamp($model->start_date);
            $model->end = Yii::$app->formatter->asTimestamp($model->end_date);
            $model->save();

            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionPublish($id)
    {
        $this->findModel($id)->publish();

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionUnpublish($id)
    {
        $this->findModel($id)->unpublish();

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionApprove($id)
    {
        if (!Yii::$app->user->can('canApprove')) throw new BadRequestHttpException();

        $this->findModel($id)->approve();

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionUnapprove($id)
    {
        if (!Yii::$app->user->can('canApprove')) throw new BadRequestHttpException();

        $this->findModel($id)->unapprove();

        return $this->redirect(Yii::$app->request->referrer);
    }

    private function findModel($id)
    {
        if (($model = Vacation::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
