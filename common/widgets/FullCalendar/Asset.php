<?php

namespace common\widgets\FullCalendar;

use yii\web\AssetBundle;

class Asset extends AssetBundle
{
    public $sourcePath = '@common/widgets/FullCalendar/assets';

    public $css = [
        'packages/core/main.min.css',
        'packages/daygrid/main.css',
        'packages/list/main.min.css',
        'css/main.css',
    ];

    public $js = [
        'packages/core/main.min.js',
        'packages/core/locales-all.min.js',
        'packages/interaction/main.min.js',
        'packages/daygrid/main.min.js',
        'packages/list/main.min.js',
        'js/main.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
    ];
}
