<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Vacation;

class VacationSearch extends Vacation
{
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Vacation::find()
            ->alias('v')
            ->joinWith('user u');

        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'start' => SORT_ASC,
                ]
            ]
        ]);

        /**
         * Настройка параметров сортировки
         * Важно: должна быть выполнена раньше $this->load($params)
         */
        $dataProvider->setSort([
            'attributes' => [
                'username',
                'start_date' => [
                    'asc' => ['start' => SORT_ASC],
                    'desc' => ['start' => SORT_DESC],
                ],
                'end_date' => [
                    'asc' => ['end' => SORT_ASC],
                    'desc' => ['end' => SORT_DESC],
                ],
                'status',
                'approve',
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        if ($this->start_date) {
            $query->andFilterWhere(['>=', 'start', Yii::$app->formatter->asTimestamp($this->start_date)]);
        }
        if ($this->end_date) {
            $query->andFilterWhere(['<=', 'end', Yii::$app->formatter->asTimestamp($this->end_date)]);
        }

        $query
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['v.status' => $this->status])
            ->andFilterWhere(['v.approve' => $this->approve])
        ;

        return $dataProvider;
    }
}
