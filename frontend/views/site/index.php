<?php

use yii\helpers\Html;
use common\widgets\FullCalendar\Widget as FullCalendarWidget;

use common\widgets\FullCalendar\Asset as FullCalendarAsset;
FullCalendarAsset::register($this);

$this->title = 'План отпусков';
?>

<div class="site-index">
    <div class="site-index__greeting">
        <?php if (Yii::$app->user->isGuest): ?>
            <h4>Для добавления и редактирования дат необходимо <?= Html::a('авторизоваться', ['/site/login']) ?></h4>
        <?php else: ?>
            <h4>Добро пожаловать, <?= $userName ?></h4>
        <?php endif; ?>
    </div>

    <div class="site-index__calendar">
        <?=FullCalendarWidget::widget([
    		'userId' => $userId,
    		'userName' => $userName,
        ]) ?>
    </div>
</div>
