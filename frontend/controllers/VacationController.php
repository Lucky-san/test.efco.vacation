<?php
namespace frontend\controllers;

use Yii;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use common\models\Vacation;

class VacationController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [],
            ],
        ];
    }

    public function actionGetEvents()
    {
        $events = Vacation::getEvents();
        $result = $this->makeEvents($events);

        return $this->asJson($result);
    }

    public function actionAddEvent()
    {
        if (Yii::$app->user->isGuest) throw new BadRequestHttpException();

        $request = Yii::$app->request;
        $start = +$request->get('start');
        $end = +$request->get('end');

        if ($start > 0 && $end > 0) {
            $model = new Vacation();

            $model->user_id = Yii::$app->user->id;
            $model->start = $start;
            $model->end = $end;
            $model->status = 1;
            $model->approve = 0;
            $model->save();

            $result = [
                'status' => 'ok',
                'id' => $model->id,
            ];

            return $this->asJson((object)$result);
        }
    }

    public function actionUpdateEvent()
    {
        if (Yii::$app->user->isGuest) throw new BadRequestHttpException();

        $request = Yii::$app->request;
        $id = +$request->get('id');
        $start = +$request->get('start');
        $end = +$request->get('end');

        if ($id > 0 && $start > 0 && $end > 0) {
            $model = $this->findModel($id);

            $model->start = $start;
            $model->end = $end;
            $model->save();

            $result = [
                'status' => 'ok',
            ];

            return $this->asJson((object)$result);
        }
    }

    private function makeEvents($events)
    {
        $result = [];

        foreach ($events as $event) {
            $isAdmin = Yii::$app->user->can('admin');
            $isOwner = Yii::$app->user->id === +$event['user_id'];
            $isApprove = $event['approve'];
            $color = $this->getEventColor();
            $isEditable = $isAdmin || ($isOwner && !$isApprove) ? true : false;
            $title = $event['user']['username'];
            if ($isApprove) $title .= ' (принято)';

            $result[] = [
                'id' => $event['id'],
                'uid' => $event['user_id'],
                'title' => $title,
                'start' => date('Y-m-d', $event['start']),
                'end' => date('Y-m-d', $event['end']),
                'color' => $color,
                'editable' => $isEditable,
            ];
        }

        return $result;
    }

    private function getEventColor()
    {
        static $index = 0;

        $colors = [
            'f7412c','ec1562','9d1db2','6834bc','3f4db8',
            '1294f6','00a7f6','00bcd7','009788','48b14c',
            '89c541','cdde20','feed19','fec200','fe9900',
            'ff5608','7b5548','9e9e9e','607d8d',
        ];

        if (count($colors) - 1 < $index) $index = 0;

        return '#' . $colors[$index++];
    }

    private function findModel($id)
    {
        if (($model = Vacation::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
