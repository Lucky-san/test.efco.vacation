<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'authEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
];
