<?php

namespace common\widgets\FullCalendar;

use yii\base\Widget as BaseWidget;

class Widget extends BaseWidget
{
    public $userId;
    public $userName;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('index', [
          'userId' => $this->userId,
          'userName' => $this->userName,
        ]);
    }
}
