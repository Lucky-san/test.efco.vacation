<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

$this->title = 'Отпуска';
?>

<div>
    <div>
        <?= Html::a('<i class="fa fa-download" aria-hidden="true"></i> Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => ['class' => 'grid-view grid-view_body-center'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'username',
                'value' => function($data) {
                    return $data->user->username;
                },
            ],
            [
                'attribute' => 'start_date',
                'value' => function($data) {
                    return Yii::$app->formatter->asDate($data->start, 'php:Y-m-d');
                },
            ],
            [
                'attribute' => 'end_date',
                'value' => function($data) {
                    return Yii::$app->formatter->asDate($data->end, 'php:Y-m-d');
                },
            ],
            [
                'attribute' => 'status',
                'value' => function($data) {
                    return ($data->status === 1)
                        ? '<span class="label label-success">ОПУБЛИКОВАНО</span>'
                        : '<span class="label label-danger">НЕ ОПУБЛИКОВАНО</span>';
                },
                'format' => 'raw',
                'filter' => [
                    '0' => 'Не опубликовано',
                    '1' => 'Опубликовано',
                ],
            ],
            [
                'attribute' => 'approve',
                'value' => function($data) {
                    return ($data->approve === 1)
                        ? '<span class="label label-success">ПРИНЯТО</span>'
                        : '<span class="label label-danger">НЕ ПРИНЯТО</span>';
                },
                'format' => 'raw',
                'filter' => [
                    '0' => 'Не принято',
                    '1' => 'Принято',
                ],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Действия',
                'template' => '{view} {update} {publish} {unpublish} {approve} {unapprove} {delete}',
                'visibleButtons' => [
                    'view' => false,
                    'update' => function($model) {
                        return $model->checkActionAccess() ? true : false;
                    },
                    'delete' => function($model) {
                        return $model->checkActionAccess() ? true : false;
                    },
                    'publish' => function($model) {
                        if (!$model->checkActionAccess()) return false;
                        return ($model->status === 1) ? true : false;
                    },
                    'unpublish' => function($model) {
                        if (!$model->checkActionAccess()) return false;
                        return ($model->status === 0) ? true : false;
                    },
                    'approve' => function($model) {
                        if (!Yii::$app->user->can('admin')) return false;
                        return ($model->approve === 1) ? true : false;
                    },
                    'unapprove' => function($model) {
                        if (!Yii::$app->user->can('admin')) return false;
                        return ($model->approve === 0) ? true : false;
                    },
                ],
                'buttons' => [
                    'publish' => function($url, $model, $key) {
                        return Html::a('', Url::to(['unpublish', 'id' => $model->id]), [
                            'class' => 'glyphicon glyphicon-remove',
                            'title' => 'Снять публикацию',
                            'data' => [
                                'confirm' => 'Вы уверены, что хотите снять с публикации?',
                                'method' => 'post',
                            ],
                        ]);
                    },
                    'unpublish' => function($url, $model, $key) {
                        return Html::a('', Url::to(['publish', 'id' => $model->id]), [
                            'class' => 'glyphicon glyphicon-send',
                            'title' => 'Опубликовать',
                            'data' => [
                                'confirm' => 'Вы уверены, что хотите опубликовать?',
                                'method' => 'post',
                            ],
                        ]);
                    },
                    'approve' => function($url, $model, $key) {
                        return Html::a('', Url::to(['unapprove', 'id' => $model->id]), [
                            'class' => 'glyphicon glyphicon-remove',
                            'title' => 'Снять принятие',
                            'data' => [
                                'confirm' => 'Вы уверены, что хотите снять принятие?',
                                'method' => 'post',
                            ],
                        ]);
                    },
                    'unapprove' => function($url, $model, $key) {
                        return Html::a('', Url::to(['approve', 'id' => $model->id]), [
                            'class' => 'glyphicon glyphicon-send',
                            'title' => 'Принять',
                            'data' => [
                                'confirm' => 'Вы уверены, что хотите принять?',
                                'method' => 'post',
                            ],
                        ]);
                    },
                ]
            ],
        ]
    ]) ?>
</div>
